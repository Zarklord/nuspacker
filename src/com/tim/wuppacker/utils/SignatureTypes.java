package com.tim.wuppacker.utils;

public class SignatureTypes {
    public enum Types{
        unknown1(0x00010004);
        private final int type;
        Types(int type) { this.type = type; }
        public int getValue() { return type; }
    }
}
