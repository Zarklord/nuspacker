package com.tim.wuppacker;

public class ContentDetails {
    private boolean isContent = true;
    private boolean isEncrypted = true;
    private boolean isHashed = false;
    private short groupID = 0x0000;
    private long parentTitleID = 0x0;
    
    private short entriesFlag = 0x0000;
    
    private int targetSize = 0;
    
    public ContentDetails(boolean isHashed,short groupID,long parentTitleID,short entriesFlags,int targetSize){
        setHashed(isHashed);
        setGroupID(groupID);
        setParentTitleID(parentTitleID);
        setEntriesFlag(entriesFlags);
        setTargetSize((targetSize/0x10000)*0xfc00);
    }
    
    public ContentDetails(boolean isHashed,short groupID,long parentTitleID,short entriesFlags) {
        this(isHashed, groupID, parentTitleID, entriesFlags,0);
    }

    public boolean isHashed() {
        return isHashed;
    }
    public void setHashed(boolean isHashed) {
        this.isHashed = isHashed;
    }
    public short getGroupID() {
        return groupID;
    }
    public void setGroupID(short groupID) {
        this.groupID = groupID;
    }
    public long getParentTitleID() {
        return parentTitleID;
    }
    public void setParentTitleID(long parentTitleID) {
        this.parentTitleID = parentTitleID;
    }
    public boolean isContent() {
        return isContent;
    }
    public void setContent(boolean isContent) {
        this.isContent = isContent;
    }
    public boolean isEncrypted() {
        return isEncrypted;
    }
    public void setEncrypted(boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public short getEntriesFlag() {
        return entriesFlag;
    }

    public void setEntriesFlag(short entriesFlag) {
        this.entriesFlag = entriesFlag;
    }
    
    public int getTargetSize() {
        return targetSize;
    }

    public void setTargetSize(int targetSize) {
        this.targetSize = targetSize;
    }
}
