package com.tim.wuppacker;

import java.io.FileOutputStream;
import java.io.IOException;

import com.tim.wuppacker.fst.FST;
import com.tim.wuppacker.tmd.ContentInfo;
import com.tim.wuppacker.tmd.TMD;
import com.tim.wuppacker.utils.Cert;
import com.tim.wuppacker.utils.Content;
import com.tim.wuppacker.utils.HashUtil;
import com.tim.wuppacker.utils.Ticket;

public class NUSPackage {
    private Ticket ticket;
    private TMD tmd;
    private FST fst;
    
    private String outputdir = "output";

    public void packContents(String outputDir) {
        if(outputDir != null && !outputDir.isEmpty()){
            setOutputdir(outputDir);
        }
        System.out.println("Pack contents.");
        getFST().getContents().packContents(outputDir); //Do this before creating the title.tmd.
        
        //Set the correct FST hash and size.
        Content fstContent = fst.getContents().getFSTContent();
        fstContent.setHash(HashUtil.hashSHA1(fst.getAsData()));
        fstContent.setSize(fst.getAsData().length);
        
        //Update the grouphash
        ContentInfo contentInfo = tmd.getContentInfos().getContentInfo(0);
        contentInfo.setHash(HashUtil.hashSHA2(tmd.getContents().getAsData()));
        //And the tmd contentinfo hash
        tmd.updateContentInfoHash();
        
        try {
            
            FileOutputStream fos = new FileOutputStream("fst.bin");
            fos.write(fst.getAsData());
            fos.close();
       
            fos = new FileOutputStream(getOutputdir() + "/title.tmd");
            fos.write(tmd.getAsData());
            fos.close(); 
      
            System.out.println("TMD saved to  "+ getOutputdir() + "/title.tmd");
             // Currently resulting in a "Failed to match cached cert"-error. We need to use a legit ticket
            fos = new FileOutputStream(getOutputdir() + "/title.cert");
            fos.write(Cert.getCertAsData());
            fos.close(); 
        
            System.out.println("Cert saved to output/title.cert");
        
        
            fos = new FileOutputStream(getOutputdir() + "/title.tik");
            fos.write(ticket.getAsData());
            fos.close(); 
            System.out.println("Ticket saved to " + getOutputdir() + "/title.tik");
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getOutputdir() {
        return outputdir;
    }

    public void setOutputdir(String outputdir) {
        this.outputdir = outputdir;
    }

    public TMD getTMD() {
        return tmd;
    }

    public void setTMD(TMD tmd) {
        this.tmd = tmd;
    }

    public FST getFST() {
        return fst;
    }

    public void setFST(FST fst) {
        this.fst = fst;
    }

    public void printTicketInfos(){
        System.out.println("Encrypted with this key           : " + getTicket().getDecryptedKey());
        System.out.println("Key encrypted with this key       : " + getTicket().getEncryptWith());
        System.out.println();
        System.out.println("Encrypted key                     : " + getTicket().getEncryptedKey());
    }
}
