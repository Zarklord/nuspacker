package com.tim.wuppacker.tmd;

import java.nio.ByteBuffer;

import com.tim.wuppacker.utils.IHasData;

public class ContentInfo implements IHasData{
    private short indexOffset   = 0x00;
    private short commandCount  = 0x0B;
    private byte[] SHA2 = new byte[0x20]; //Will be patched anyway.
    
    public ContentInfo() {
        this((short) 0);
    }
 
    public ContentInfo(short contentCount) {
        this((short) 0,contentCount);
    }
    public ContentInfo(short indexOffset,short contentCount) {
        this.indexOffset = indexOffset;
        this.commandCount = contentCount;
    }    
   
    @Override
    public byte[] getAsData(){
        ByteBuffer buffer = ByteBuffer.allocate(0x24);
        buffer.putShort(indexOffset);
        buffer.putShort(commandCount);        
        buffer.put(SHA2);        
        return buffer.array();
    }
    
    public static int getDataSizeStatic() {        
        return 0x24;
    }    

    @Override
    public int getDataSize() {        
        return 0x24;
    }

    public int getCommandCount() {
        return commandCount;
    }

    public void setHash(byte[] hash) {
       this.SHA2 = hash;
    }
}
