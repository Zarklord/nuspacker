package com.tim.wuppacker;

import java.io.File;

import com.tim.wuppacker.utils.Key;
import com.tim.wuppacker.utils.Utils;

public class Starter {
    public static void main (String args[]){       
        System.out.println("NUSPacker 0.1 - alpha");
        System.out.println();
        
        new File("tmp").mkdir();
        
        String inputPath = "output";
        String outputPath = "output";
        new File(outputPath).mkdir();
        
        long titleID = 0x0;
        
        String encryptionKey = "";
        String encryptKeyWith = "";      
        
        for(int i = 0;i<args.length;i++){
            if(args[i].equals("-in")){
                if(args.length > i){
                    inputPath = args[i+1];
                    i++;
                }
            }else if(args[i].equals("-out")){
                if(args.length > i){
                    outputPath = args[i+1];
                    new File(outputPath).mkdir();
                    i++;
                }
            }else if(args[i].equals("-tID")){
                if(args.length > i){
                    titleID = Utils.StringToLong(args[i+1]);
                    i++;
                }
            }else if(args[i].equals("-encryptionKey")){
                if(args.length > i){
                    encryptionKey = args[i+1];
                    i++;
                }
            }else if(args[i].equals("-encryptKeyWith")){
                if(args.length > i){
                    encryptKeyWith = args[i+1];
                    i++;
                }
            }
        }
        
        short content_group = (short) ((titleID >> 8) & 0xFFFF);
        //content_group = 0x06D32;
        
        ContentRules rules = new ContentRules();
        
        //I'm not sure of the order of the content. Maybe you can arrange it in the way we want. But this is working =)
        
        //At first we have the code .xml's
        ContentDetails common_details_code =  new ContentDetails(false, (short) 0x0, 0x0L,(short) 0x0000); // not hashed, groupID empty, parentid empty, fstentry flags
        /*00000001*/ rules.createNewRule("/code/app.xml",common_details_code);
        /*00000002*/ rules.createNewRule("/code/cos.xml",common_details_code);
        
        //Then the meta.xml
        ContentDetails common_details_meta =  new ContentDetails(true, (short) 0x0400, 0x0L,(short) 0x0040/*,0x00030000*/);// // hashed, groupID 0x400, parentid empty, fstentry flags   
        /*00000003*/ rules.createNewRule("/meta/meta.xml", common_details_meta);
        
        //Then the rest of the meta folder except the meta.xml
        common_details_meta =  new ContentDetails(true, (short) 0x0400, 0x0L,(short) 0x0040/*,0x00B30000*/);// // hashed, groupID 0x400, parentid empty, fstentry flags   
        /*00000004*/ rules.createNewRule("/meta/.*[^.xml]+", common_details_meta);
        
        //But lets move the bootMovie + Logo in own files.
        common_details_meta =  new ContentDetails(true, (short) 0x0400, 0x0L,(short) 0x0040/*,0x00110000*/);// // hashed, groupID 0x400, parentid empty, fstentry flags  
        /*00000005*/ rules.createNewRule("/meta/bootMovie.h264", common_details_meta);
        /*00000006*/ rules.createNewRule("/meta/bootLogoTex.tga", common_details_meta);
        
        //... and the manual 
        ContentDetails common_details_meta_manual =  new ContentDetails(true, (short) 0x0400, 0x0L,(short) 0x0040/*,0x01150000*/);// // hashed, groupID 0x400, parentid empty, fstentry flags   
        /*00000007*/ rules.createNewRule("/meta/Manual.bfma", common_details_meta_manual);        
        
        //Now we can assign the rpx and rpls. each gets it own content. just to be sure.
        /*00000008*/ rules.createNewRule("/code/.*(.rpx|.rpl)",common_details_code,true); // Each file has it own content file
        
        //Don't forget the preload.txt
        ContentDetails common_details_preload =  new ContentDetails(true, (short) 0x0000, 0x0L,(short) 0x0000);// // hashed, groupID 0x400, parentid empty, fstentry flags   
        /*000000??*/ rules.createNewRule("/code/preload.txt",common_details_preload); // Each file has it own content file
        
        //And finally the content
        ContentDetails common_details_content =  new ContentDetails(true, content_group, titleID,(short) 0x0400);// // hashed, groupID part of titleid, parentid own titleid, fstentry flags
        /*000000??*/ rules.createNewRule("/content/.*", common_details_content);
        
        NUSPackage nuspackage = NUSPackageFactory.createNewPackage(inputPath, titleID, new Key(encryptionKey), new Key(encryptKeyWith), rules);
        
        nuspackage.packContents(outputPath);
        nuspackage.printTicketInfos();
    }
}
