package com.tim.wuppacker.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import com.tim.wuppacker.ContentDetails;
import com.tim.wuppacker.NUSPackage;
import com.tim.wuppacker.NUSPackageFactory;
import com.tim.wuppacker.fst.FSTEntry;
import com.tim.wuppacker.tmd.TMD;

/**
 * Represents a content used in a package. 
 * A content holds a number of files while are saves in a .app file. 
 * The ID also used as a filename(.app)
 * @author timogus
 *
 */
public class Content implements IHasData{
	/**
	 * Represents the different types a content can be of.
	 */
	public static final short TYPE_CONTENT = 0x2000;
	public static final short TYPE_ENCRYPTED = 0x0001;
	public static final short TYPE_HASHED = 0x0002;
	/**
	 * ID of this content. Unique this package
	 */
    private int ID = 0x00;
    /**
	 * Index of this content. Unique this package
	 */
	private short index = 0x00;
	/**
	 * Type of this content
	 */
	private short type = TYPE_CONTENT & TYPE_ENCRYPTED;
	/**
	 * ???
	 */
	private long size;
	/**
	 * If type = OneFile, the hash is the SHA1 hash of the decrypted file (Filled with 0x00 until its aligned to 0x8000 (aka filesize mutiple of 0x8000)).
	 * If type = MutiplyHashedFiles, the hash is the SHA1 hash of the corresponding .h3 file.
	 */
	private byte[] SHA2 = new byte[0x14];
	
	
	/**
     * Current fileoffset
     */
    private int curFileOffset = 0;
    private static final int BLOCKSIZE                  = 0x20;
    private static final int END_WITH_BLOCKSIZE_HASHED  = 0x10000;
    private static final int END_WITH_BLOCKSIZE         = 0x08000;
    
	/**
	 * FSTEntries that are in this content
	 */
    private List<FSTEntry> entries = new ArrayList<>();
    
    /**
     * GroupID of this Content
     */
    private int groupID = 0;
    
    /**
     * parentTitleID of this Content. Is the games when this package is an Update. But not for all contents ~
     */
    private long parentTitleID = 0;
    
    private short entriesFlags = 0x0000;
    
    private int targetSize = 0x0;
    
    
	Content() {        
        
    }
	
	public Content(ContentDetails details) {
        // TODO Auto-generated constructor stub
    }

    /**
	 * Returns the ID of the content
	 * @return
	 */
	public int getID(){
	    return this.ID;
	}
	
	/**
	 * Sets the ID
	 * @param id
	 */
    public void setID(int id){
	    this.ID = id;
	}
	
    /**
     * Returns the type
     * @return
     */
    public short getType() {
        return type;
    }
    
    /**
     * Adds the type
     * @param type
     */
    public void addType(short type) {
        this.type |= type;
    }
    /**
     * Adds the type
     * @param type
     */
    public void removeType(short type) {
        this.type &= ~type;
    }

    /**
     * Sets the type
     * @param type
     */
    public void setType(short type) {
        this.type = type;
    }

    /**
     * Returns the Index
     * @return
     */
    public short getIndex() {
        return index;
    }

    /**
     * Sets the index
     * @param index
     */
	public void setIndex(short index) {
        this.index = index;
        
    }
	
    public long getParentTitleID() {
        return parentTitleID;
    }

    public void setParentTitleID(long parentTitleID) {
        this.parentTitleID = parentTitleID;
    }

    @Override
    public byte[] getAsData() {
        ByteBuffer buffer = ByteBuffer.allocate(getDataSize());
        buffer.putInt(ID);
        buffer.putShort(index);
        buffer.putShort(type);
        buffer.putLong(size);
        buffer.put(SHA2);
        return buffer.array();
    }
    
    @Override
    public int getDataSize() {        
        return 0x30;
    }
    
    public List<FSTEntry> getAllFiles(){
    	List<FSTEntry> files = new ArrayList<>();
    	for(FSTEntry entry : entries){
    		if(entry.isFile()){
    			files.add(entry);
    		}
    	}
		return files;
    }
    
    /**
     * 
     * @param entries flat list of all FSTEntry contained in this content
     */
    public void update(List<FSTEntry> entries){
    	//At first update our local List.
    	if(entries != null){
    		this.entries = entries;
    	}
    }
    
    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

	public void setSize(int size) {
		this.size = size;
	}

	public void setHash(byte[] hash) {
        this.SHA2 = hash;
    }
	
	public byte[] getHash() {
       return this.SHA2;
    }

    public int getFSTContentHeaderDataSize() {
		return 0x20;
	}
    
    public static int content_offset = 0;
	public byte[] getFSTContentHeaderAsData() {
	    ByteBuffer buffer = ByteBuffer.allocate(0x20);
      
        byte unkwn = 0;
        int fst_content_size = (int) (size / 0x8000);
        int fst_content_size_written = fst_content_size;
       
        
        if((getType() & TYPE_HASHED) == TYPE_HASHED){
            unkwn = 2;
            fst_content_size_written -= ((fst_content_size / 64)+1)*2; //Hopefully this is right

            //fst_content_size_written = (int) (size / 0x8400);
        }else{
            unkwn = 1;
        }
        
        
        buffer.putInt(content_offset);
        buffer.putInt(fst_content_size_written);
        
        NUSPackage nusPackage = NUSPackageFactory.getPackageByContent(this);        
        
        if(this.equals(nusPackage.getFST().getContents().getFSTContent())){
            unkwn = 0;
            buffer.put(0x14,(byte) 0);
            /**
             * Totally guessing here.
             */
            if(fst_content_size == 1){
                fst_content_size = 0;
            }
            content_offset += fst_content_size + 2;
            fst_content_size = 0;
        }else{
            content_offset += fst_content_size;
        }
           
        buffer.putLong(getParentTitleID());
        buffer.putInt(0x10,getGroupID());
        buffer.put(0x14,(byte) unkwn);      //Seems be if this content is Hashes or not. But always 0 for the FST
      
        return buffer.array();
	}

    public int addFileSize(FSTEntry fstEntry) {
        int old_fileoffset = curFileOffset;
        size = old_fileoffset +  fstEntry.getFilesize();        
        curFileOffset += Utils.align((int) fstEntry.getFilesize(), BLOCKSIZE);
       
        return old_fileoffset;
    }


    public void resetFileOffsets() {
        curFileOffset = 0;
    }
    
    @Override
    public boolean equals(Object other){
        boolean result;
        if((other == null) || (getClass() != other.getClass())){
            result = false;
        }else{
            Content other_ = (Content)other;
            result = ID == other_.ID;
        }
        return result;
    }
    
    public void packContentToFile(String outputDir){
        if(getAllFiles().size() == 0) return;
        
     // All files of this content are hashed!!!
        for(FSTEntry child : entries){
            child.setFlags(getEntriesFlags());
        }
        
        NUSPackage nusPackage = NUSPackageFactory.getPackageByContent(this);
        TMD tmd = nusPackage.getTMD();
        
        System.out.println("Packing Content " + String.format("%08X", getID()));
        String tmp_path = "tmp/" + String.format("%08X", getID()) +".dec"; //TODO: remove tmp files. Removes hardcoded directory
	    System.out.println("Packing files in this content.");
	    int cur_offset = 0;
	    
	    FileOutputStream fos = null;
        try {
           
            fos = new FileOutputStream(tmp_path);
            long totalwritten = 0;
    	    for(FSTEntry entry : entries){
    	        if(entry.isFile()){
    	            //System.out.println("filelength :" + String.format("%08X", entry.getFilesize()) + " expected offset" + String.format("%08X", entry.getFileOffset()) + " real offset: " + String.format("%08X", cur_offset) + " filename: " +entry.getFilename());
    	            //cur_offset = (int) entry.getFileOffset();
    	            int old_offset = cur_offset;
    	            if(cur_offset != entry.getFileOffset()){
                        System.out.println("FAILED");
                    }
    	            int padding = cur_offset;
        	        cur_offset += Utils.align((int) entry.getFilesize(), BLOCKSIZE); //We add +1 to pad to the next full BLOCKSIZE
        	        padding = (int) (cur_offset - (padding + entry.getFilesize()));        	        
        	        
        	        totalwritten += Utils.copyFileInto(entry.getFile(),fos);
        	        totalwritten += padding;
                    fos.write(new byte[padding]);
                    System.out.println("Wrote at " + String.format("%08x",old_offset) + " | FileSize: " + String.format("%08x",entry.getFilesize()) + " | " + entry.getFilename() );
    	        }
    	    }
    	    if(getTargetSize() != 0){
                int padding = (int) (getTargetSize()-10  - totalwritten);
                System.out.println(totalwritten + " " + padding + " " + (totalwritten + padding) + " aaaaaaaa");
                fos.write(new byte[padding]);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }finally{
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
    	if(!((getType() & TYPE_HASHED) == TYPE_HASHED)){
    		//We need to calculate the hash!
    	    tmp_path = "tmp/" + String.format("%08X", getID()) +".dec"; //TODO: remove tmp files. Removes hardcoded directory
    	    File decrypted_file = new File(tmp_path);
    		setHash(HashUtil.hashSHA1(decrypted_file,END_WITH_BLOCKSIZE));
    		Encryption encryption = tmd.getEncryption();
    		
    		try {
                encryption.encryptFileWithPadding(decrypted_file,this,outputDir + "/" + String.format("%08X.app", getID()),END_WITH_BLOCKSIZE);
            } catch (IOException e) {
                e.printStackTrace();
            }
    	}else{
    	    tmp_path = "tmp/" + String.format("%08X", getID()) +".dec"; //TODO: remove tmp files. Removes hardcoded directory
    		File decrpyted_file = new File(tmp_path);
    		    		
            System.out.println("Let's generate the hashes");
            ContentHashes hashes = new ContentHashes(decrpyted_file);
      
            System.out.println("Now we need to encrypt it.");
            
            Encryption encryption = tmd.getEncryption();
            String fileoutput = outputDir + "/" + String.format("%08X", getID()) + ".app";
            try {
                encryption.encryptFileHashed(new File(tmp_path),this,fileoutput,hashes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            System.out.println("Encrypted =)");
            
            tmp_path = outputDir + "/" + String.format("%08X", getID()) +".h3";
            System.out.println(tmp_path);
           
            try {
                fos = new FileOutputStream(tmp_path);
                fos.write(hashes.getH3Hashes());
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //Set hash to the hash of the h3
            setHash(HashUtil.hashSHA1(hashes.getH3Hashes()));
                      
            System.out.println("Generating .h3");
    	}
    	tmp_path = outputDir + "/" + String.format("%08X", getID()) +".app";
    	File encrypted_file = new File(tmp_path);
    	
		setSize((int)encrypted_file.length());
        System.out.println("Content " + String.format("%08X", getID()) + " packed!");
        System.out.println("-------------");
    }

    public void setEntriesFlags(short entriesFlag) {
       this.entriesFlags =  entriesFlag;
    }

    public short getEntriesFlags() {
        return entriesFlags;
    }

    public int getTargetSize() {
        return targetSize;
    }

    public void setTargetSize(int targetSize) {
        this.targetSize = targetSize;
    }

    public int getFSTEntryNumber() {
       return entries.size();
    }
}