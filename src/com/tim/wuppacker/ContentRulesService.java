package com.tim.wuppacker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tim.wuppacker.fst.FSTEntry;
import com.tim.wuppacker.utils.Content;
import com.tim.wuppacker.utils.Contents;



public class ContentRulesService {

    public static void applyRules(FSTEntry root,final Contents targetContents, ContentRules rules) {
        System.out.println("-----");
        for(ContentRule rule :rules.getRules()){
            System.out.println("Apply rule " + rule.getPattern());
            if(rule.isContentPerMatch()){
                setNewContentRecursiveRule("",rule.getPattern(),root, targetContents,rule);
            }else{
                Content cur_content = targetContents.getNewContent(rule.getDetails());
                boolean result = setContentRecursiveRule("",rule.getPattern(), root, cur_content);
                if(!result){
                    System.out.println("No file matched the rule. Lets delete the content again");
                    targetContents.deleteContent(cur_content);
                }
            }
            System.out.println("-----");
        }
    }
    
    private static Content setNewContentRecursiveRule(String path, final String pattern, FSTEntry cur_entry,final Contents targetContents, ContentRule rule) {
        path += cur_entry.getFilename() + "/";
        Pattern p = Pattern.compile(pattern);
        Content result = null;
        
        if(cur_entry.getChildren().size() == 0){
            String filePath = path;
            Matcher m = p.matcher(filePath);
            System.out.println("Trying " + pattern + "to" + filePath);
            if(m.matches()){
                Content result_content = targetContents.getNewContent(rule.getDetails());                
                //System.out.println("Set content to " + String.format("%08X", cur_content.getID()) + " for: " + filePath);
                //child.setContent(cur_content);
                result = result_content;
            }
        }
        for(FSTEntry child: cur_entry.getChildren()){
            if(child.isDir()){
                Content child_result = setNewContentRecursiveRule(path,pattern,child,targetContents,rule);
                if(child_result != null){
                    result = child_result;
                }
            }else{
                String filePath = path + child.getFilename();
                Matcher m = p.matcher(filePath);
                if(m.matches()){    
                    Content result_content = targetContents.getNewContent(rule.getDetails());
                    System.out.println("Set content to " + String.format("%08X", result_content.getID()) + " for: " + filePath);
                    child.setContent(result_content);
                    result = result_content;
                }
            }           
        }
        if(result != null){
            cur_entry.setContent(result);
        }
        return result;
        
    }

    private static boolean setContentRecursiveRule(String path,final String pattern,FSTEntry cur_entry, Content cur_content){
        path += cur_entry.getFilename() + "/";
        Pattern p = Pattern.compile(pattern);
        boolean result = false;
        if(cur_entry.getChildren().size() == 0){
            String filePath = path;
            Matcher m = p.matcher(filePath);
            //System.out.println("Trying " + pattern + "to" + filePath);
            if(m.matches()){
                System.out.println("Set content to " + String.format("%08X", cur_content.getID()) + " for: " + filePath);
                //child.setContent(cur_content);
                return true;
            }else{
                return false;
            }
        }
        for(FSTEntry child: cur_entry.getChildren()){
            if(child.isDir()){
                boolean child_result = setContentRecursiveRule(path,pattern,child,cur_content);
                if(child_result){
                    cur_entry.setContent(cur_content);
                    result = true;
                }
            }else{
                String filePath = path + child.getFilename();
                Matcher m = p.matcher(filePath);
                //System.out.println("Trying " + pattern + "to" + filePath);
                if(m.matches()){
                    System.out.println("Set content to " + String.format("%08X", cur_content.getID()) + " for: " + filePath);
                    child.setContent(cur_content);
                    result = true;
                }
            }           
        }
        if(result){
            cur_entry.setContent(cur_content);
        }
        return result;
    }

}
