package com.tim.wuppacker.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;

public class Utils {
    public static long align(long input, int alignment){
        long newSize = (input/alignment);
        if(newSize * alignment != input){
            newSize++;
        }
        newSize = newSize * alignment;        
        return newSize;        
    }
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    
    public static long StringToLong(String s) {
        try{
            BigInteger bi = new BigInteger(s, 16);          
            return bi.longValue();
        }catch(NumberFormatException e){
            System.err.println("Invalid Title ID");
            return 0L;
        }
    }
    
    public static String ByteArrayToString(byte[] ba)
    {
      if(ba == null) return null;
      StringBuilder hex = new StringBuilder(ba.length * 2);
      for(byte b : ba){
        hex.append(String.format("%02X", b));
      }
      return hex.toString();
    }
    
    public static int getChunkFromStream(InputStream inputStream,byte[] output, ByteArrayBuffer overflowbuffer,int excptedSize) throws IOException {
		int bytesRead = -1;
    	int inBlockBuffer = 0;
    	do{
    		bytesRead = inputStream.read(overflowbuffer.buffer,overflowbuffer.getLengthOfDataInBuffer(),overflowbuffer.getSpaceLeft());    		
    		if(bytesRead <= 0) break;

    		overflowbuffer.addLengthOfDataInBuffer(bytesRead);
	    	
	    	if(inBlockBuffer + overflowbuffer.getLengthOfDataInBuffer() > excptedSize){
	    		int tooMuch = (inBlockBuffer + bytesRead) - excptedSize;
	    		int toRead = excptedSize - inBlockBuffer;
	    		
	    		System.arraycopy(overflowbuffer.buffer, 0, output, inBlockBuffer, toRead);
	    		inBlockBuffer += toRead;
	    		
	    		System.arraycopy(overflowbuffer.buffer, toRead, overflowbuffer.buffer, 0, tooMuch);
	    		overflowbuffer.setLengthOfDataInBuffer(tooMuch);
	    	}else{     
	    		System.arraycopy(overflowbuffer.buffer, 0, output, inBlockBuffer, overflowbuffer.getLengthOfDataInBuffer()); 
	    		inBlockBuffer +=overflowbuffer.getLengthOfDataInBuffer();
	    		overflowbuffer.resetLengthOfDataInBuffer();
	    	}
    	}while(inBlockBuffer != excptedSize);
    	return inBlockBuffer;
    }
    
    public static long copyFileInto(File file, OutputStream out) throws IOException {
        FileInputStream in =  new FileInputStream(file);
        long written = 0;
        long filesize = file.length();
        int buffer_size = 0x10000;
        byte[] buffer = new byte[buffer_size];
        do{
            int read = in.read(buffer);
            out.write(buffer,0,read);
            written += read;
            
        }while(written < filesize);
        
        in.close();
        return written;
    }

}
