package com.tim.wuppacker.tmd;

import java.nio.ByteBuffer;

import com.tim.wuppacker.utils.IHasData;

public class ContentInfos implements IHasData{
    private ContentInfo[] contentinfos = new ContentInfo[0x40];
    public ContentInfos(){
        
    }
    public void setContentInfo(int index, ContentInfo contentInfo) {
        if(index < 0 && index > (contentinfos.length-1)){
            throw new IllegalArgumentException("Error on setting ContentInfo, index " + index + " invalid");
        }
        if(contentInfo == null){
            throw new IllegalArgumentException("Error on setting ContentInfo, ContentInfo is null");
        }
        contentinfos[index] = contentInfo;
    }
    
    public ContentInfo getContentInfo(int i) { //TODO: add boundary check.
        if(contentinfos[i] == null){
            contentinfos[i] =  new ContentInfo();
        }
        return contentinfos[i];
    }
    
    public byte[] getAsData(){
        ByteBuffer buffer = ByteBuffer.allocate(ContentInfo.getDataSizeStatic() * contentinfos.length);
        for(int i = 0;i<contentinfos.length-1;i++){
            if(contentinfos[i] == null) contentinfos[i] = new ContentInfo();
            buffer.put(contentinfos[i].getAsData());
        }
        return buffer.array();
    }
    
    @Override
    public int getDataSize() {        
        return contentinfos.length * ContentInfo.getDataSizeStatic();
    }
   
}
