package com.tim.wuppacker.utils;
/**
 * A interface for the serialized data
 * @author timogus
 *
 */
public interface IHasData {
    public byte[] getAsData();    
    public int getDataSize();
}
