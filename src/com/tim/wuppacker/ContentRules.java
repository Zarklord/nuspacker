package com.tim.wuppacker;

import java.util.ArrayList;
import java.util.List;

public class ContentRules {
    public List<ContentRule> rules = new ArrayList<>();
    
    public ContentRules(){
        
    }
    
    public List<ContentRule> getRules(){
        return rules;
    }
    
    public ContentRule addRule(ContentRule rule){
        if(!rules.contains(rule)){
            rules.add(rule);
        }
        return rule;
    }
    
    public ContentRule createNewRule(String pattern,ContentDetails details,boolean contentPerMatch){
        ContentRule newRule = new ContentRule(pattern, details,contentPerMatch);
        rules.add(newRule);
        return newRule;
    }

    public ContentRule createNewRule(String pattern, ContentDetails details) {
        return createNewRule(pattern, details, false);
    }
}
