package com.tim.wuppacker.tmd;

import java.nio.ByteBuffer;
import java.util.concurrent.ThreadLocalRandom;

import com.tim.wuppacker.fst.FST;
import com.tim.wuppacker.utils.Cert;
import com.tim.wuppacker.utils.Contents;
import com.tim.wuppacker.utils.Encryption;
import com.tim.wuppacker.utils.HashUtil;
import com.tim.wuppacker.utils.IHasData;
import com.tim.wuppacker.utils.IV;
import com.tim.wuppacker.utils.Key;
import com.tim.wuppacker.utils.SignatureTypes;
import com.tim.wuppacker.utils.Ticket;
import com.tim.wuppacker.utils.Utils;

public class TMD implements IHasData{
	private int signatureType = SignatureTypes.Types.unknown1.getValue();  //0x000
	private byte[] signature = new byte[0x100];                            //0x004
	private byte[] padding0 = new byte[0x3C];                              //0x104
	private byte[] issuer = ByteBuffer.allocate(0x40).put(Utils.hexStringToByteArray("526F6F742D434130303030303030332D435030303030303030620000000000000000000000000000000000000000000000000000000000000000000000000000")).array();                                //0x140

	private byte version = 0x01;                                           //0x180
	private byte CACRLVersion = 0x00;                                      //0x181
	private byte signerCRLVersion = 0x00;                                  //0x182
	private byte padding1 = 0x00;                                          //0x183
	
	private long systemVersion = 0x000500101000400AL;                      //0x184
	//private long titleID = 0x0000000000000000L;                          //0x18C
	private int	titleType = 0x000100;                                      //0x194
	private short groupID = 0x1079;                                        //0x198
	private int app_type = 0x80000000;          //for updates 0x0800001B;   //0x19A
	private int random1 = 0;
	private int random2 = 0;//0x02FE6000; //something about the (encrypted) sizes?
	private byte[] reserved = new byte[50];                               
	private int accessRights = 0x0000;                                     //0x1D8
	private short titleVersion = 0x00;                                     //0x1DC
	private short contentCount = 0x00;                                     //0x1DE
	private short bootIndex = 0x00;                                        //0x1E0
	private byte[] padding3 = new byte[2];                                 //0x1E2
	private byte[] SHA2 = new byte[0x20];                                  //0x1E4
	
	private ContentInfos contentInfos =  null;
	private Contents contents = null;
	private byte[] certs = Cert.getTMDCertAsData();
	
	private Ticket ticket;
	
	public TMD(FST fst,Ticket ticket){
	    setTicket(ticket);
	    setContents(fst.getContents());
	    contentInfos = new ContentInfos();
	}

    private void setContents(Contents contents){
	    if(contents != null){
    		this.contents = contents;
    		contentCount = contents.getContentCount();
	    }
	}
	
	public void update(){
	    updateContents();
	}
	
	public void updateContents(){
	    this.contentCount = (short) (contents.getContentCount());
	    
	    ContentInfo firstContentInfo = new ContentInfo(contents.getContentCount());
	    byte [] randomHash  = new byte[0x20];
        ThreadLocalRandom.current().nextBytes(randomHash);
        
        firstContentInfo.setHash(HashUtil.hashSHA2(contents.getAsData()));
        getContentInfos().setContentInfo(0,firstContentInfo);
	}

    public void updateContentInfoHash() {
        this.SHA2 = HashUtil.hashSHA2(getContentInfos().getAsData());
    }

    @Override
    public byte[] getAsData() {
        ByteBuffer buffer = ByteBuffer.allocate(getDataSize());
        buffer.putInt(signatureType);
        buffer.put(signature);
        buffer.put(padding0);
        buffer.put(issuer);
        
        buffer.put(version);
        buffer.put(CACRLVersion);
        buffer.put(signerCRLVersion);
        buffer.put(padding1);
       
        buffer.putLong(systemVersion);
        buffer.putLong(getTicket().getTitleID());
        buffer.putInt(titleType);
        buffer.putShort(groupID);
        buffer.putInt(app_type);
        buffer.putInt(random1);
        buffer.putInt(random2);
        buffer.put(reserved);
        buffer.putInt(accessRights);
        buffer.putShort(titleVersion);
        buffer.putShort(contentCount);
        buffer.putShort(bootIndex);
        
        buffer.put(padding3);
        buffer.put(SHA2);
        
        buffer.put(getContentInfos().getAsData());
        buffer.put(getContents().getAsData());
        //buffer.put(certs); not needed
        return buffer.array();
    }

    @Override
    public int getDataSize() {
        int staticSize = 0x204;
        int contentInfoSize = contentInfos.getDataSize();
        int contentsSize = contents.getDataSize();
        //int certSize = certs.length;
        return staticSize + contentInfoSize + contentsSize;// + certSize;
    }

    public ContentInfos getContentInfos() {
        if(contentInfos == null){
            contentInfos = new ContentInfos();
        }
        return contentInfos;
    }

    public void setContentInfos(ContentInfos contentInfos) {
        this.contentInfos = contentInfos;
    }

    public Contents getContents() {
        if(contents == null){
            contents = new Contents();
        }
        return contents;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
    
    public Encryption getEncryption() {
        ByteBuffer iv = ByteBuffer.allocate(0x10);
        iv.putLong(getTicket().getTitleID());
        Key key = getTicket().getDecryptedKey();        
        return  new Encryption(key,new IV(iv.array()));
    }
}
