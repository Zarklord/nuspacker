package com.tim.wuppacker;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.tim.wuppacker.fst.FST;
import com.tim.wuppacker.fst.FSTEntries;
import com.tim.wuppacker.fst.FSTEntry;
import com.tim.wuppacker.tmd.TMD;
import com.tim.wuppacker.utils.Content;
import com.tim.wuppacker.utils.Contents;
import com.tim.wuppacker.utils.Key;
import com.tim.wuppacker.utils.Ticket;

public class NUSPackageFactory {
    private static Map<Content,NUSPackage> contentDictionary = new HashMap<>();
    private static Map<FST,NUSPackage> FSTDictionary = new HashMap<>();
    private static Map<TMD,NUSPackage> TMDDictionary = new HashMap<>();
    private static Map<FSTEntries,NUSPackage> FSTEntriesDictionary = new HashMap<>();
    private static Map<Contents,NUSPackage> contentsDictionary = new HashMap<>();
    
    
    public static NUSPackage createNewPackage(String dir,long titleID,Key encryptionKey,Key encryptKeyWith,ContentRules rules){
        NUSPackage nusPackage = new NUSPackage();
        
        Contents contents = new Contents();
        FST fst = new FST(contents);
        addFSTDictonary(fst,nusPackage);
        FSTEntries entries =  fst.getFSTEntries();
        addFSTEntriesDictonary(fst.getFSTEntries(),nusPackage);  
        
        FSTEntry root =  entries.getRootEntry();
        root.setContent(contents.getFSTContent());
        
        //Create FSTEntries for the given directory.
        File dir_read = new File(dir);
        readFiles(dir_read.listFiles(),root);
      
        System.out.println("Files read. Set it to content files.");
        
        ContentRulesService.applyRules(root,contents,rules);
        
        addContentsDictonary(contents,nusPackage);
        addContentDictonary(contents,nusPackage);
        
        System.out.println("Generating the FST.");
        fst.update();
        
        System.out.println("Generating the Ticket.");
        
        // titleid, key used for encryption, key used for encrypting the key.
        Ticket ticket = new Ticket(titleID,encryptionKey, encryptKeyWith);
        
        System.out.println("Creating the TMD.");
        TMD tmd = new TMD(fst,ticket);       
        tmd.update();
       
        addTMDDictonary(tmd,nusPackage);
      
        
        nusPackage.setFST(fst);
        nusPackage.setTicket(ticket);
        nusPackage.setTMD(tmd);
        
        //System.out.println(root);
        return nusPackage;
    }
    
    private static void addContentsDictonary(Contents contents, NUSPackage nusPackage) {
        contentsDictionary.put(contents, nusPackage);
    }
    private static void addContentDictonary(Contents contents, NUSPackage nusPackage) {
        for(Content c: contents.getContents()){
            if(!contentDictionary.containsKey(c)){
                contentDictionary.put(c, nusPackage);
            }
        }
    }
    
    private static void addTMDDictonary(TMD tmd, NUSPackage nusPackage) {       
        TMDDictionary.put(tmd, nusPackage);        
    }

    private static void addFSTDictonary(FST fst, NUSPackage nusPackage) {     
        FSTDictionary.put(fst, nusPackage);        
    }
    
    private static void addFSTEntriesDictonary(FSTEntries fstEntries, NUSPackage nusPackage) {     
        FSTEntriesDictionary.put(fstEntries, nusPackage);        
    }

    public static NUSPackage getPackageByContent(Content content){
        if(contentDictionary.containsKey(content)){
            return contentDictionary.get(content);
        }
        return null;
    }
    
    public static NUSPackage getPackageByFST(FST fst){
        if(FSTDictionary.containsKey(fst)){
            return FSTDictionary.get(fst);
        }
        return null;
    }
    
    public static NUSPackage getPackageByTMD(TMD tmd){
        if(TMDDictionary.containsKey(tmd)){
            return TMDDictionary.get(tmd);
        }
        return null;
    }
    
    public static NUSPackage getPackageByContents(Contents contents) {
        if(contentsDictionary.containsKey(contents)){
            return contentsDictionary.get(contents);
        }
        return null;
    }

    public static NUSPackage getPackageByFSTEntires(FSTEntries fstEntries) {
        if(FSTEntriesDictionary.containsKey(fstEntries)){
            return FSTEntriesDictionary.get(fstEntries);
        }
        return null;
    }
    
    public static void readFiles(File[] list,FSTEntry parent){     
        for(File f : list){
            if(f.isDirectory()){
                FSTEntry newdir = new FSTEntry(f);
                parent.addChildren(newdir);
                readFiles(f.listFiles(),newdir);
            }
        }
        for(File f : list){
            if(!f.isDirectory()){                
                parent.addChildren(new FSTEntry(f));                
            }
        }   
       
    }
}
